/*======================================================================*/
/*
 * Main entry program of json packer using json-c and APR.
 */
/*----------------------------------------------------------------------*/
//#include <json.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "hash.h"
#include "json_work.h"

/*----------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
	/* Input file descriptor */
	FILE *inputf = NULL;
	/* Output file descriptors */
	FILE *outputtxt = NULL;
	FILE *outputbin = NULL;

	/*json parsing object */
	struct json_object *jobj;

	/*APR Objects for use of the Hash Table */
	apr_pool_t *mp = NULL;
	apr_hash_t *ht = NULL;

	char *theBuffer = NULL;
	size_t bufsiz;
	ssize_t nbytes;

	/* First check the parameters */
	if (argc > 1) {
		int res = check_parameters(argc, argv, &inputf);
		if (0 > res) {
			fprintf(stderr, "Incorrect Parameters!\n");
			return -1;
		} else if (0 < res)
			return -2;
	}

	/* If there is no input file the Entry will be by stdin */
	if (NULL == inputf)
		inputf = stdin;

	/* Initialization of Hash Table and APR Memory Pool */
	ht = init_hash(&mp);

	if (NULL == mp || NULL == ht) {
		fprintf(stderr, "Error Initializing APR! ABORT!\n");
		return -3;
	}

	outputbin = fopen("TLVEncode.bin", "wb");

	/* JSON Parsing and Packing Procedure */
	int totalBytes = 0;
	while ((nbytes = getline(&theBuffer, &bufsiz, inputf)) != -1) {
		jobj = json_tokener_parse(theBuffer);

		int keysAdded = add_keys_to_hashtable(jobj, mp, ht);
		totalBytes += generate_TLV_file(jobj, mp, ht, outputbin);
		if (isVerbose) {
			if (NULL == jobj) {
				printf("Malformed JSON:\"%s\"", theBuffer);
			} else {
				printf("Adding %d elements to hashtable.\n",
				       keysAdded);
				printf("Inserting %d bytes to file.\n",
				       totalBytes);
			}
		}

		free(theBuffer);
		theBuffer = NULL;
	}

	if (isVerbose)
		printf("Total Bytes written: %d\n", totalBytes);

	fclose(inputf);
	fclose(outputbin);

	outputtxt = fopen("Dictionary.txt", "w");
	if (NULL == outputtxt)
		outputtxt = stdout;
	write_hashtable_to_file_as_txt(ht, outputtxt);
	fclose(outputtxt);

	return 0;
}

/*======================================================================*/
