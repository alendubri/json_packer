/*======================================================================*/
/*
 * Main entry program of TEST json packer using json-c and APR.
 * These are White Box Test Mainly. I did not use TDD this time
 * because I was new to APR and json-c libraries.
 */
/*----------------------------------------------------------------------*/
//#include <json.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include "utils.h"
#include "hash.h"
#include "json_work.h"

static FILE *tfile = NULL;

/*APR Objects for use of the Hash Table */
apr_pool_t *mp = NULL;
apr_hash_t *ht = NULL;

/*----------------------------------------------------------------------*/
int init_suite_1(void)
{
	isVerbose = FALSE;
	return 0;
}

/*----------------------------------------------------------------------*/
int init_suite_2(void)
{
	mp = NULL;
	ht = NULL;
	return 0;
}

/*----------------------------------------------------------------------*/
int clean_suite_2(void)
{
	return 0;
}

/*----------------------------------------------------------------------*/
int clean_suite_1(void)
{
	if (NULL == tfile)
		return 0;

	if (0 != fclose(tfile)) {
		return -1;
	} else {
		tfile = NULL;
		return 0;
	}
}

/*----------------------------------------------------------------------*/
void test_check_parameters_h(void)
{
	char *argv[] = { "runprog", "-h" };
	int argc = 2;

	isVerbose = FALSE;
	CU_ASSERT(1 == check_parameters(argc, argv, &tfile));
	CU_ASSERT(FALSE == isVerbose);
	CU_ASSERT(NULL == tfile);
}

void test_check_parameters_v(void)
{
	char *argv[] = { "runprog", "-v" };
	int argc = 2;

	isVerbose = FALSE;
	CU_ASSERT(0 == check_parameters(argc, argv, &tfile));
	CU_ASSERT(TRUE == isVerbose);
	CU_ASSERT(NULL == tfile);
}

void test_check_parameters_any(void)
{
	char *argv[] = { "runprog", "any" };
	int argc = 2;

	isVerbose = FALSE;
	CU_ASSERT(-1 == check_parameters(argc, argv, &tfile));
	CU_ASSERT(FALSE == isVerbose);
	CU_ASSERT(NULL == tfile);
}

void test_check_parameters_none(void)
{
	char *argv[] = { "runprog" };
	int argc = 1;

	isVerbose = FALSE;
	CU_ASSERT(-1 == check_parameters(argc, argv, &tfile));
	CU_ASSERT(FALSE == isVerbose);
	CU_ASSERT(NULL == tfile);
}

void test_check_parameters_f_no_file(void)
{
	char *argv[] = { "runprog", "-f" };
	int argc = 2;

	isVerbose = FALSE;
	CU_ASSERT(-1 == check_parameters(argc, argv, &tfile));
	CU_ASSERT(FALSE == isVerbose);
	CU_ASSERT(NULL == tfile);
}

void test_check_parameters_f_file(void)
{
	char *argv[] = { "runprog", "-f", "input1.json" };
	int argc = 3;

	isVerbose = FALSE;
	CU_ASSERT(0 == check_parameters(argc, argv, &tfile));
	CU_ASSERT(FALSE == isVerbose);
	CU_ASSERT(NULL != tfile);
	if (NULL != tfile) {
		fclose(tfile);
		tfile = NULL;
	}
}

/*----------------------------------------------------------------------*/
void test_init_hash(void)
{
	CU_ASSERT(NULL == mp);
	CU_ASSERT(NULL == ht);

	ht = init_hash(&mp);
	CU_ASSERT(NULL != mp);
	CU_ASSERT(NULL != ht);
}

void test_close_mp(void)
{
	CU_ASSERT(NULL != mp);
	CU_ASSERT(NULL != ht);

	close_mp(mp);
	CU_ASSERT(NULL == mp);
		/** AD: I am wondering why the closing of APR is not clearing these
		 * Pointers. Here both asserts fails!
		 */
	CU_ASSERT(NULL == ht);
}

/*----------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
	CU_pSuite pSuite1 = NULL;
	CU_pSuite pSuite2 = NULL;

	if (CUE_SUCCESS != CU_initialize_registry()) {
		fprintf(stderr, "ERROR initializing CUnit Framework!");
		return CU_get_error();
	}

	/* add first suite to the registry */
	pSuite1 = CU_add_suite("Suite_1", init_suite_1, clean_suite_1);
	if (NULL == pSuite1) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	if ((NULL ==
	     CU_add_test(pSuite1, "test of check_parameters() param -h\n",
			 test_check_parameters_h))
	    || (NULL ==
		CU_add_test(pSuite1, "test of check_parameters() param -v\n",
			    test_check_parameters_v))
	    || (NULL ==
		CU_add_test(pSuite1, "test of check_parameters() no param\n",
			    test_check_parameters_none))
	    || (NULL ==
		CU_add_test(pSuite1,
			    "test of check_parameters() param -f, no file\n",
			    test_check_parameters_f_no_file))
	    || (NULL ==
		CU_add_test(pSuite1,
			    "test of check_parameters() param -f, with file\n",
			    test_check_parameters_f_file))
	    || (NULL ==
		CU_add_test(pSuite1, "test of check_parameters() param any\n",
			    test_check_parameters_any))) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	/* add Second suite to the registry */
	pSuite2 = CU_add_suite("Suite_2", init_suite_2, clean_suite_2);
	if (NULL == pSuite1) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	if ((NULL ==
	     CU_add_test(pSuite2, "test of Init APR Memory Pool and Hash\n",
			 test_init_hash)) ||
	    (NULL ==
	     CU_add_test(pSuite2,
			 "test of Closing and Liberating APR Memory Pool and Hash\n",
			 test_close_mp))) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	/* Run all tests using the CUnit Basic interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}

/*======================================================================*/
