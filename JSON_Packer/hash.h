/*======================================================================*/
/**
 * Definitions for APR and Hastable functionality
 */
#ifndef _HASH_H_
#define _HASH_H_

#include <apr_general.h>
#include <apr_hash.h>
#include <apr_strings.h>

apr_hash_t *init_hash(apr_pool_t ** mp);

void close_mp(apr_pool_t * mp);

void write_hashtable_to_file_as_txt(apr_hash_t * ht, FILE * outp);

#endif
/*======================================================================*/
