/*======================================================================*/
/**
 * Definitions for Utility Functions.
 */
#ifndef _UTILS_H_
#define _UTILS_H_

#define TRUE  1
#define FALSE 0

extern unsigned char isVerbose;

void print_help();

int check_parameters(int argc, char *argv[], FILE ** inputf);

#endif
/*======================================================================*/
