/*======================================================================*/
#ifndef _JSON_WORK_H_
#define _JSON_WORK_H_

#include <apr_general.h>
#include <apr_hash.h>
#include <apr_strings.h>
#include <json.h>

/**
 * I will propose a TLV Encoding that begins with the JSON type. Based on
 * json_type enum I did this other enum that I could compress in a 4 bit
 * segment of the TLV
 */
typedef enum packer_type {
	packer_type_null,
	packer_type_boolean,
	packer_type_double,
	packer_type_int,
	/*
	 * They are not needed for this implementation.
	 packer_type_object,
	 packer_type_array,
	 */
	packer_type_string
} t_packer_type;

/* This Structure is the Basis of the TLV for the binary output.
 * There are limitations in this design, but the problem specification
 * is not clear about it.
 * These limitations are:
 * Only 256 types (must be less)
 * Only 65535 different Keys (maybe more are needed)
 * The Payload must be at maximun 65535 bytes (maybe more are needed)
 */
typedef struct stTLV {
	unsigned char type;
	unsigned short keynr;
	unsigned short plsize;
	void *payload;
} t_stTLV;

/* End Of Record Definition for writing in TLV File */
#define EOR 0xFF

int add_keys_to_hashtable(json_object * jobj, apr_pool_t * mp, apr_hash_t * ht);
int generate_TLV_file(json_object * jobj, apr_pool_t * mp, apr_hash_t * ht,
		      FILE * outputbin);

#endif
/*======================================================================*/
