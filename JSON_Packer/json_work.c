/*======================================================================*/
/**
 * Functions to read the JSON file and manipulate the hashtable.
 * Also here is created the resulting TLV File
 */
/*----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "json_work.h"
#include "hash.h"

/*----------------------------------------------------------------------*/
/**
 * 
 */
int add_keys_to_hashtable(json_object * jobj, apr_pool_t * mp, apr_hash_t * ht)
{
	static int keyCnt = 0;
	int retVal = 0;

	if (NULL == jobj) {
		fprintf(stderr, "Malformed JSON!\n");
	} else {
		int insertedKeys = keyCnt;
		json_object_object_foreach(jobj, key, val) {
			const int *val =
			    apr_hash_get(ht, key, APR_HASH_KEY_STRING);
			if (NULL == val) {
				keyCnt++;
				int *tmpVal = apr_palloc(mp, sizeof(int));
				*tmpVal = keyCnt;
				apr_hash_set(ht, apr_pstrdup(mp, key),
					     APR_HASH_KEY_STRING, tmpVal);
			}
		}
		/* Returns the total of keys inserted */
		retVal = (keyCnt - insertedKeys);
	}

	return retVal;
}

/*----------------------------------------------------------------------*/
/**
 * 
 */
int generate_TLV_file(json_object * jobj, apr_pool_t * mp, apr_hash_t * ht,
		      FILE * outputbin)
{
	int retVal = 0;
	t_stTLV tmpTVL;

	if (NULL == jobj || NULL == outputbin) {
		fprintf(stderr, "Malformed JSON or output file not open!\n");
	} else {
		enum json_type type;
		json_object_object_foreach(jobj, key, val) {	/*Passing through every array element */
			const int *hval =
			    apr_hash_get(ht, key, APR_HASH_KEY_STRING);
			tmpTVL.keynr = *hval;
			type = json_object_get_type(val);
			switch (type) {
			case json_type_boolean:
				/* NOTE: for Boolean I encode using unsigned char to use the less 
				   space possible in the resulting file 
				 */
				tmpTVL.type = packer_type_boolean;
				tmpTVL.plsize = sizeof(unsigned char);
				unsigned char bval =
				    (unsigned char)json_object_get_boolean(val);
				tmpTVL.payload = &bval;
				break;
			case json_type_double:
				tmpTVL.type = packer_type_double;
				tmpTVL.plsize = sizeof(double);
				double dblval = json_object_get_double(val);
				tmpTVL.payload = &dblval;
				break;
			case json_type_int:
				tmpTVL.type = packer_type_int;
				tmpTVL.plsize = sizeof(int);
				int ival = json_object_get_int(val);
				tmpTVL.payload = &ival;
				break;
			case json_type_string:
				/* NOTE: The encoded string does not have the trailing \0, the 
				   string size is recreated by the payload byte
				 */
				tmpTVL.type = packer_type_string;
				const char *stval = json_object_get_string(val);
				tmpTVL.plsize = strlen(stval);
				tmpTVL.payload = (void *)stval;
				break;
			default:
				fprintf(stderr, "JSON Type not supported!");
				break;
			}
			retVal +=
			    (fwrite
			     (&tmpTVL.type, sizeof(unsigned char), 1,
			      outputbin) * sizeof(unsigned char));
			retVal +=
			    (fwrite
			     (&tmpTVL.keynr, sizeof(unsigned short), 1,
			      outputbin) * sizeof(unsigned short));
			retVal +=
			    (fwrite
			     (&tmpTVL.plsize, sizeof(unsigned short), 1,
			      outputbin) * sizeof(unsigned short));
			retVal +=
			    (fwrite(tmpTVL.payload, tmpTVL.plsize, 1, outputbin)
			     * tmpTVL.plsize);
		}
		/*The symbol EOR (Actually 0xFF) will define the End of evey JSON Record */
		unsigned char eor = EOR;
		retVal +=
		    (fwrite(&eor, sizeof(unsigned char), 1, outputbin) *
		     sizeof(unsigned char));

	}

	return retVal;
}

/*======================================================================*/
