/*======================================================================*/
/**
 * Functions to manipulate the APR Hashtable Object.
 */
/*----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#include "hash.h"

/*----------------------------------------------------------------------*/
/**
 * Traversing the Hashtable and printing it contents in outp
 */
void write_hashtable_to_file_as_txt(apr_hash_t * ht, FILE * outp)
{
	apr_hash_index_t *hi;
	int cnt = 1;
	for (hi = apr_hash_first(NULL, ht); hi; hi = apr_hash_next(hi), cnt++) {
		const char *key;
		void *val;

		apr_hash_this(hi, (const void **)&key, NULL, &val);
		fprintf(outp, "\"%s\":%d", key, *(int *)val);
		if (cnt < apr_hash_count(ht))
			fprintf(outp, ",");
		if (0 == (cnt % 4))	// Make a line break every 4 elements.
			fprintf(outp, "\n");
	}
	fprintf(outp, "\n");
}

/*----------------------------------------------------------------------*/
/**
 * Encapsulating APR Library Initialization: Memory Pool and Hashtable.
 */
apr_hash_t *init_hash(apr_pool_t ** mp)
{
	apr_hash_t *ht = NULL;

	apr_initialize();
	apr_pool_create(mp, NULL);

	if (mp)
		ht = apr_hash_make(*mp);

	return ht;
}

/*----------------------------------------------------------------------*/
/**
 * Gracefully closing APR
 */
void close_mp(apr_pool_t * mp)
{
	/* the hash table is destroyed when @mp is destroyed */
	if (mp)
		apr_pool_destroy(mp);

	apr_terminate();
}

/*======================================================================*/
