
/*======================================================================*/
/**
 * Utility Functions.
 */
/*----------------------------------------------------------------------*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "utils.h"

/*----------------------------------------------------------------------*/
unsigned char isVerbose = FALSE;
/*----------------------------------------------------------------------*/
/**
 * Print the Help on stdout
 */
void print_help()
{
	printf("JSON_Packer [-f InputFile.json]\n");
	printf
	    ("Calling the program without parameter will read every line in the console\n");
	printf("until you press CTRL-D.\n");
	printf
	    ("-f needs the extra parameter InputFile.json. It will read the JSON from that file.\n");
	printf("-v will show some verbosity during run.\n");
	printf("-h will show this message and exit.\n");
}

/*----------------------------------------------------------------------*/
/**
 * A very simplistic parameter parser. 
 */
int check_parameters(int argc, char *argv[], FILE ** inputf)
{
	int retVal = -1;
	for (int i = 1; i < argc; i++) {
		if (0 == strcmp("-f", argv[i])) {
			*inputf = fopen(argv[i + 1], "r");
			if (NULL != *inputf) {
				retVal = 0;
				break;
			}
		} else if (0 == strcmp("-h", argv[i])) {
			retVal = 1;
			print_help();
			break;
		} else if (0 == strcmp("-v", argv[i])) {
			isVerbose = TRUE;
			retVal = 0;
		}
	}

	return retVal;
}

/*======================================================================*/
