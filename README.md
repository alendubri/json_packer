# json packer

The task is to read data in JSON format and using dictionary encoding write a compressed stream to disk.

The input will contain records in JSON format. Each record is separated by a new line (LF). The input will only contain simple types, complex and nested data structures will not be present (i.e. map and array).
Input example:

- { “key1”:“value”, “key2”:42, “key3”:TRUE}
- {“sadsf”:”dsewtew”, “dsre”:3221, “sdfds”:”dsfewew”}

Basically a record is a list of KVPs (key-value pairs).
The KVP 
Once the input is read, the program should encode the input so that the keys are put into a dictionary. The dictionary maps the keys (strings) to an integer. The key strings are then replaced with a number.
The dictionary would be as follows for the first record:

“key1”:1, “key2”:2, “key3”:3

This record is then represented as:
{ 1:“value”, 2:42, 3:TRUE}

Types supported by JSON must be handled and preserved (i.e. integer, string, boolean).  The output should be dumped to a file in a binary form using TLV encoding instead of the above text-based representation. This binary TLV encoding is more compact, e.g. a boolean value can be encoded in two bytes (or even less).

 At the end of the process the dictionary must be also written to a file using the same encoding method for the string-integer pairs.

## Compilation

I was using WSL Debian GNU/Linux 11 (bullseye).

You Need to Download and install APR from [here](https://dlcdn.apache.org//apr/apr-1.7.4.tar.gz).
After Download and unpack it, you must configure and compile and install APR
```bash
cd apr-1.7.4/
./configure --prefix=/usr/local
make
sudo make install
```

You need to install the json-c package

```bash
sudo apt-get install libjson-c-dev
sudo apt-get install libjson-c-doc
```
After that you can compile the package:

```bash
cd json_packer/JSON_Packer/
make
```

## Usage

the resulting executable will be called **jsonPacker**. If you run with -h parameter it will show you this help message:

```bash
dubuc@E6440:~/NXLog/json_packer/JSON_Packer$ ./jsonPacker -h
JSON_Packer [-f InputFile.json]
Calling the program without parameter will read every line in the console
until you press CTRL-D.
-f needs the extra parameter InputFile.json. It will read the JSON from that file.
-v will show some verbosity during run.
-h will show this message and exit.
dubuc@E6440:~/NXLog/json_packer/JSON_Packer$
```
### Ways to use the program:
- ./jsonPacker -f input.json
- cat input.json | ./jsonPacker
- ./jsonPacker < input.json
- If you call the program without any parameter it will wait for console input until you type CTRL-D.

In the development directory are 4 JSON files to test with. One of them is malformed, and the last one is empty. After run there will be 2 new files generated:

- **Dictionary.txt**: is the dictionary of keys with its correspondent numerical index
- **TLVEncode.bin**:  is the binary encoded file of the test.

## Binary TLV file description

The binary output file has the following structure that follows the KVP defined in the example JSON files:
- type: 8 bits, it has the value given by json of the 4 Basic Types.
- key: 16 bits, it is the index of the key in the key Dictionary, this relational index you can look at in the Dictionary.txt file.
- size: 16 bits, is the size if the data
- Data: variable length: depends of the type it contains the binary representation of the data.

Every set of  JSON KVP is separated by an internal binary flag of only one byte: 0xFF

## Test

There are some unit test done in **CUnit** Test Framework. This framework as installed so:

```bash
sudo apt-get install libcunit1 libcunit1-dev libcunit1-doc
```

To compile and run the test, there is a Makefile rule:

```bash
make test
./jsonPacker-test
```

There are very few test, they are White Box test. During the run of them I found problems with the APR library, because they do not nullify the Memory Pool and Hash pointers as the documentation says. In that way, I ran a valgrind analysis of the ./jsonPacker program. I use the following commands:
```bash
valgrind -s --log-file=valgrind.01.txt --leak-check=full --show-leak-kinds=all ./jsonPacker -f input1.json
valgrind -s --log-file=valgrind.02.txt --leak-check=full --show-leak-kinds=all ./jsonPacker-test
```

The resulting files are part of the next Commit.


## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.
